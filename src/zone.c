#include "zone.h"

zone_t *new_zone(void)
{
	zone_t *zone = NULL;
	
	zone = calloc(1, sizeof(zone_t));
	zone_create(zone);
	zone_select_tileset(zone);
	
	return zone;
}

void zone_create(zone_t *zone)
{
	register int width, height;
	
	for (height = 0; height < 10; height++)
	{
		for (width = 0; width < 10; width++)
		{
			if (width == 0 || width == 9 || height == 0 || height == 9)
			{
				zone->tile[height][width] = 0;
			}
			else
			{
				zone->tile[height][width] = rand() % 3 + 1;
				if (zone->tile[height][width] == 3)
				{
					if (rand() % 100 > 25)
					{
						zone->tile[height][width] = rand() % 2 + 1;
					}
				}
			}
		}
	}
}

void zone_select_tileset(zone_t *zone)
{
	switch(rand() % 4)
	{
		default:
		case 0:
			zone->tileset = images->desert;
			break;
		case 1:
			zone->tileset = images->plain;
			break;
		case 2:
			zone->tileset = images->iceland;
			break;
		case 3:
			zone->tileset = images->village;
			break;
	}
}

void zone_open_way(world_t *world, zone_t *zone)
{
	int count = 0;
	
	if (zone->top)
	{
		zone->tile[0][4] = 1;
		zone->tile[0][5] = 1;
		count++;
	}
	if (zone->left)
	{
		zone->tile[4][0] = 1;
		zone->tile[5][0] = 1;
		count++;
	}
	if (zone->right)
	{
		zone->tile[4][9] = 1;
		zone->tile[5][9] = 1;
		count++;
	}
	if (zone->bottom)
	{
		zone->tile[9][4] = 1;
		zone->tile[9][5] = 1;
		count++;
	}
	if (world->ways > count)
	{
		world->start_zone = zone;
	}
}

int zone_place_coin(zone_t *zone, int coin)
{
	register int height, width;
	
	for (height = 1; height < 10 - 1; height++)
	{
		for (width = 1; width < 10 - 1; width++)
		{
			if (zone->tile[height][width] != 0 && zone->tile[height][width] != 3 && rand() % 100 < 25)
			{
				coins[coin]->zone = zone;
				coins[coin]->position.y = height * 32 + 8;
				coins[coin]->position.x = width * 32 + 8;
				return 1;
			}
		}
	}
	return 0;
}

int zone_place_monsters(zone_t *zone, int monster)
{
	register int height, width;
	
	for (height = 1; height < 10 - 1; height++)
	{
		for (width = 1; width < 10 - 1; width++)
		{
			if (zone->tile[height][width] != 0 && zone->tile[height][width] != 3 && rand() % 100 < 25)
			{
				monsters[monster]->zone = zone;
				monsters[monster]->position.y = height * 32;
				monsters[monster]->position.x = width * 32;
				return 1;
			}
		}
	}
	return 0;
}

