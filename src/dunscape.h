#ifndef HAVE_DUNSCAPE_H
#define HAVE_DUNSCAPE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "display.h"
#include "command.h"
#include "zone.h"
#include "world.h"

void init_game(void);

coin_t *coins[12];
monster_t *monsters[7];

#endif

