#include "command.h"

void monsters_action(void)
{
	register int i;
	
	for (i = 0; i < 7; i++)
	{
		if (monsters[i]->life && monsters[i]->zone == player->actual_zone)
		{
			if ((player->position.x >= monsters[i]->position.x - 32 && player->position.x <= monsters[i]->position.x + 32) &&
				  (player->position.y >= monsters[i]->position.y - 32 && player->position.y <= monsters[i]->position.y + 32))
			{
				player->life--;
			}
			else
			{
				int posY = monsters[i]->position.y / 32;
				int posX = monsters[i]->position.x / 32;

				if (player->position.x < monsters[i]->position.x && monsters[i]->zone->tile[posY][posX - 1] != 0 && monsters[i]->zone->tile[posY][posX - 1] != 3)
				{
					monsters[i]->position.x -= 32;
					posX = monsters[i]->position.x / 32;
				}

				if (player->position.x > monsters[i]->position.x && monsters[i]->zone->tile[posY][posX + 1] != 0 && monsters[i]->zone->tile[posY][posX + 1] != 3)
				{
					monsters[i]->position.x += 32;
					posX = monsters[i]->position.x / 32;
				}

				if (player->position.y < monsters[i]->position.y && monsters[i]->zone->tile[posY - 1][posX] != 0 && monsters[i]->zone->tile[posY - 1][posX] != 3)
				{
					monsters[i]->position.y -= 32;
				}

				if (player->position.y > monsters[i]->position.y && monsters[i]->zone->tile[posY + 1][posX] != 0 && monsters[i]->zone->tile[posY + 1][posX] != 3)
				{
					monsters[i]->position.y += 32;
				}
			}
		}
	}
}

int check_monster(SDLKey key)
{
	register int i;
	
	for (i = 0; i < 7; i++)
	{
		if (monsters[i]->life && monsters[i]->zone == player->actual_zone)
		{
			if ((player->position.x >= monsters[i]->position.x - 32 && player->position.x <= monsters[i]->position.x + 32) &&
				  (player->position.y >= monsters[i]->position.y - 32 && player->position.y <= monsters[i]->position.y + 32))
			{
				switch (key)
				{
					case SDLK_z:
					case SDLK_KP1:
						if (monsters[i]->position.x == player->position.x - 32 && monsters[i]->position.y == player->position.y + 32)
						{
							monsters[i]->life--;
							return 0;
						}
					break;
					case SDLK_x:
					case SDLK_KP2:
						if (monsters[i]->position.y == player->position.y + 32 && monsters[i]->position.x == player->position.x)
						{
							monsters[i]->life--;
							return 0;
						}
					break;
					case SDLK_c:
					case SDLK_KP3:
						if (monsters[i]->position.x == player->position.x + 32 && monsters[i]->position.y == player->position.y + 32)
						{
							monsters[i]->life--;
							return 0;
						}
					break;
					case SDLK_a:
					case SDLK_KP4:
						if (monsters[i]->position.x == player->position.x - 32 && monsters[i]->position.y == player->position.y)
						{
							monsters[i]->life--;
							return 0;
						}
					break;
					case SDLK_d:
					case SDLK_KP6:
						if (monsters[i]->position.x == player->position.x + 32 && monsters[i]->position.y == player->position.y)
						{
							monsters[i]->life--;
							return 0;
						}
					break;
					case SDLK_q:
					case SDLK_KP7:
						if (monsters[i]->position.x == player->position.x - 32 && monsters[i]->position.y == player->position.y - 32)
						{
							monsters[i]->life--;
							return 0;
						}
					break;
					case SDLK_w:
					case SDLK_KP8:
						if (monsters[i]->position.y == player->position.y - 32 && monsters[i]->position.x == player->position.x)
						{
							monsters[i]->life--;
							return 0;
						}
					break;
					case SDLK_e:
					case SDLK_KP9:
						if (monsters[i]->position.x == player->position.x + 32 && monsters[i]->position.y == player->position.y - 32)
						{
							monsters[i]->life--;
							return 0;
						}
					break;
					default:
					break;
				}
			}
		}
	}
	return 1;
}

void action(void)
{
	SDL_Event event;
	zone_t *zone = player->actual_zone;
	int posY = player->position.y / 32;
	int posX = player->position.x / 32;

	SDL_WaitEvent(&event);
	
	if (player->coins == 12)
	{
		SDL_BlitSurface(images->victory, NULL, screen, NULL);
		SDL_Flip(screen);
		SDL_WaitEvent(&event);
		exit(0);
	}
	
	if (player->life <= 0)
	{
		SDL_BlitSurface(images->fail, NULL, screen, NULL);
		SDL_Flip(screen);
		SDL_WaitEvent(&event);
		exit(0);
	}
	
	if (event.type == SDL_QUIT)
	{
		input->quit = 1;
	}
	
	if (event.type == SDL_KEYDOWN)
	{
		switch (event.key.keysym.sym)
		{
			case SDLK_z:
			case SDLK_KP1:
				if (zone->tile[posY + 1][posX - 1] != 0 && zone->tile[posY + 1][posX - 1] != 3 && check_monster(SDLK_KP1))
				{
					if (player->position.y < 320 - player->animation.h && player->position.x > 0)
					{
						player->position.y += 32;
						player->position.x -= 32;
					}
				}
				break;
			case SDLK_x:
			case SDLK_KP2:
				if (zone->tile[posY + 1][posX] != 0 && zone->tile[posY + 1][posX] != 3 && check_monster(SDLK_KP2))
				{
					if (player->position.y < 320 - player->animation.h)
					{
						player->position.y += 32;
					}
				}
				break;
			case SDLK_c:
			case SDLK_KP3:
				if (zone->tile[posY + 1][posX + 1] != 0 && zone->tile[posY + 1][posX + 1] != 3 && check_monster(SDLK_KP3))
				{
					if (player->position.y < 320 - player->animation.h && player->position.x < 320 - player->animation.w)
					{
						player->position.x += 32;
						player->position.y += 32;
					}
				}
				break;
			case SDLK_a:
			case SDLK_KP4:
				if (zone->tile[posY][posX - 1] != 0 && zone->tile[posY][posX - 1] != 3 && check_monster(SDLK_KP4))
				{
					if (player->position.x > 0)
					{
						player->position.x -= 32;
					}
				}
				break;
			case SDLK_s:
			case SDLK_KP5:
				check_monster(SDLK_KP5);
				input->wait = 1;
				break;
			case SDLK_d:
			case SDLK_KP6:
				if (zone->tile[posY][posX + 1] != 0 && zone->tile[posY][posX + 1] != 3 && check_monster(SDLK_KP6))
				{
					if (player->position.x < 320 - player->animation.w)
					{
						player->position.x += 32;
					}
				}
				break;
			case SDLK_q:
			case SDLK_KP7:
				if (zone->tile[posY - 1][posX - 1] != 0 && zone->tile[posY - 1][posX - 1] != 3 && check_monster(SDLK_KP7))
				{
					if (player->position.x > 0 && player->position.y > 0)
					{
						player->position.x -= 32;
						player->position.y -= 32;
					}
				}
				break;
			case SDLK_w:
			case SDLK_KP8:
				if (zone->tile[posY - 1][posX] != 0 && zone->tile[posY - 1][posX] != 3 && check_monster(SDLK_KP8))
				{
					if (player->position.y > 0)
					{
						player->position.y -= 32;
					}
				}
				break;
			case SDLK_e:
			case SDLK_KP9:
				if (zone->tile[posY - 1][posX + 1] != 0 && zone->tile[posY - 1][posX + 1] != 3 && check_monster(SDLK_KP9))
				{
					if (player->position.y > 0 && player->position.x < 320 - player->animation.w)
					{
						player->position.x += 32;
						player->position.y -= 32;
					}
				}
				break;
			default:
				input->nothing = 1;
		}
		
		if (!input->nothing)
		{
			if (!input->wait)
			{
				check_place();
				check_coin();
			}
			monsters_action();
			display_print_zone(player->actual_zone);
			display_life();
			display_coins();
			display_interface();
			display_refresh();
		}
		input->nothing = 0;
		input->wait = 0;
	}
}

void check_place(void)
{
	if (player->position.x == 0)
	{
		player->actual_zone = player->actual_zone->left;
		player->position.x = 320 - 32;
	}
	else
	{
		if (player->position.x == 320 - 32)
		{
			player->actual_zone = player->actual_zone->right;
			player->position.x = 0;
		}
		else
		{
			if (player->position.y == 0)
			{
				player->actual_zone = player->actual_zone->top;
				player->position.y = 320 - 32;
			}
			else
			{
				if (player->position.y == 320 - 32)
				{
					player->actual_zone = player->actual_zone->bottom;
					player->position.y = 0;
				}
			}
		}
	}
}

void check_coin(void)
{
	register int i;
	
	for (i = 0; i < 12; i++)
	{
		if (!coins[i]->get && coins[i]->zone == player->actual_zone)
		{
			if (coins[i]->position.x - 8 == player->position.x && coins[i]->position.y - 8 == player->position.y)
			{
				coins[i]->get = 1;
				player->coins += 1;
				player->life += 2;
				if (player->life > 10)
				{
					player->life = 10;
				}
			}
		}
	}
}


