#ifndef HAVE_ZONE_H
#define HAVE_ZONE_H

#include <stdlib.h>

typedef struct zone_s zone_t;

#include "display.h"
#include "world.h"

struct zone_s
{
	SDL_Surface *tileset;
	char tile[10][10];
	zone_t *top;
	zone_t *left;
	zone_t *right;
	zone_t *bottom;
};

zone_t *new_zone(void);
void zone_create(zone_t *zone);
void zone_select_tileset(zone_t *zone);
void zone_open_way(world_t *world, zone_t *zone);
int zone_place_coin(zone_t *zone, int coin);
int zone_place_monsters(zone_t *zone, int monster);

#endif

