#include "dunscape.h"

int main(int argc, char *argv[])
{
	world = NULL;
	input = NULL;
	player = NULL;
	
	init_game();
	world = new_world();
	player->actual_zone = world->start_zone;
	
	display_print_zone(player->actual_zone);
	display_life();
	display_coins();
	display_interface();
	display_refresh();
	while(!input->quit)
	{
		action();
	}
	
	return EXIT_SUCCESS;
	printf("%s\n", argv[argc -1]);
}

void init_game(void)
{
	register int i;
	srand(time(NULL));
	input = calloc(1, sizeof(input_t));	
	player = calloc(1, sizeof(player_t));
	
	for (i = 0; i < 12; i++)
	{
		coins[i] = calloc(1, sizeof(coin_t));
		coins[i]->animation.h = 16;
		coins[i]->animation.w = 16;
		coins[i]->animation.x = 0;
		coins[i]->animation.y = 16;
	}
	
	for (i = 0; i < 7; i++)
	{
		monsters[i] = calloc(1, sizeof(monster_t));
		monsters[i]->animation.h = 32;
		monsters[i]->animation.w = 32;
		monsters[i]->animation.x = 32;
		monsters[i]->animation.y = 0;
		monsters[i]->life = 5;
	}

	player->position.h = 32;
	player->position.w = 32;
	player->position.x = 160;
	player->position.y = 160;
	player->animation.h = 32;
	player->animation.w = 32;
	player->life = 10;
	player->coins = 0;
	
	new_display();
}

