#ifndef HAVE_COMMAND_H
#define HAVE_COMMAND_H

typedef struct input_s input_t;
typedef struct player_s player_t;
typedef struct monster_s monster_t;

#include "display.h"
#include "world.h"

struct input_s
{
	char key[SDLK_LAST];
	int quit;
	int nothing;
	int wait;
};

struct player_s
{
	SDL_Surface *sprite;
	zone_t *actual_zone;
	SDL_Rect position;
	SDL_Rect animation;
	int life;
	int coins;
};

struct monster_s
{
	SDL_Surface *sprite;
	zone_t *zone;
	SDL_Rect position;
	SDL_Rect animation;
	int life;
};

void action(void);
void monsters_action(void);
void check_place(void);
void check_coin(void);
int check_monster(SDLKey key);

input_t *input;
player_t *player;

#endif

