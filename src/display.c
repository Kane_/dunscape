#include "display.h"

void new_display(void)
{
	screen = NULL;
	
	#ifdef WIN32
		SDL_putenv("SDL_VIDEODRIVER=directx");
	#endif
	
	SDL_Init(SDL_INIT_VIDEO);
	screen = SDL_SetVideoMode(320 + 160, 320, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
	SDL_WM_SetCaption("Dunscape", "Dunscape");
	display_load_images();
}

void display_refresh(void)
{
	unsigned int delay;
	unsigned int elapsed;
	unsigned int lasttime;

	delay = 20;
	lasttime = SDL_GetTicks();
	
	while(SDL_Flip(screen) != 0)
	{
		SDL_Delay(1);
	}

	elapsed = SDL_GetTicks()-lasttime;
	if (elapsed < delay)
	{
		SDL_Delay(delay - elapsed);
	}
}

void display_load_images(void)
{
	images = calloc(1, sizeof(images_t));
	
	player->sprite = SDL_LoadBMP("./images/player.bmp");
	images->interface = SDL_LoadBMP("./images/interface.bmp");
	images->misc = SDL_LoadBMP("./images/misc.bmp");
	images->desert = SDL_LoadBMP("./images/desert.bmp");
	images->plain = SDL_LoadBMP("./images/plain.bmp");
	images->iceland = SDL_LoadBMP("./images/iceland.bmp");
	images->village = SDL_LoadBMP("./images/village.bmp");
	images->victory = SDL_LoadBMP("./images/victory.bmp");
	images->fail = SDL_LoadBMP("./images/fail.bmp");
	SDL_SetColorKey(player->sprite, SDL_SRCCOLORKEY, SDL_MapRGB(player->sprite->format, 255, 0, 255));
	SDL_SetColorKey(images->misc, SDL_SRCCOLORKEY, SDL_MapRGB(images->misc->format, 255, 0, 255));
}

void display_interface(void)
{
	SDL_Rect dst;
	dst.y = 0;
	dst.x = 320;
	dst.h = images->interface->h;
	dst.w = images->interface->w;
	SDL_BlitSurface(images->interface, NULL, screen, &dst);
}

void display_life(void)
{
	SDL_Rect src, dst;
	int i, max, rest;
	
	src.h = 16;
	src.w = 16;
	dst.h = 16;
	dst.w = 16;
	src.x = 0;
	src.y = 0;
	dst.x = 16;
	dst.y = 16;
	
	max = player->life / 2;
	rest = player->life % 2;
	for (i = 0; i < max; i++)
	{
		SDL_BlitSurface(images->misc, &src, images->interface, &dst);
		dst.x += 16;
	}
	
	if (max < 5)
	{
		switch (rest)
		{
			default:
			case 0:
				src.x = 32;
				SDL_BlitSurface(images->misc, &src, images->interface, &dst);
				break;
			case 1:
				src.x = 16;
				SDL_BlitSurface(images->misc, &src, images->interface, &dst);
				break;
		}
		max++;
	}
	while (max < 5)
	{
		dst.x += 16;
		src.x = 32;
		SDL_BlitSurface(images->misc, &src, images->interface, &dst);
		max++;
	}
}

void display_coins(void)
{
	SDL_Rect dst;
	int i;
	
	dst.h = 16;
	dst.w = 16;
	
	for (i = 0; i < player->coins; i++)
	{
		dst.x = 32;
		if (i < 6)
		{
			dst.y = 48;
			dst.x = dst.x + i * 16;
			SDL_BlitSurface(images->misc, &coins[0]->animation, images->interface, &dst);
		}
		else
		{
			dst.y = 64;
			dst.x = dst.x + i % 6 * 16;
			SDL_BlitSurface(images->misc, &coins[0]->animation, images->interface, &dst);
		}
	}
}

void display_print_zone(zone_t *zone)
{
	register int i, width, height;
	SDL_Rect src;
	SDL_Rect dst;
	
	src.w = 32;
	src.h = 32;
	dst.w = 32;
	dst.h = 32;
	
	for (height = 0; height < 10; height++)
	{
		for (width = 0; width < 10; width++)
		{
			switch(zone->tile[height][width])
			{
				default:
				case 0:
					src.y = 0;
					src.x = 0;
					break;
				case 1:
					src.y = 0;
					src.x = src.w * 1;
					break;
				case 2:
					src.y = 0;
					src.x = src.w * 2;
					break;
				case 3:
					src.y = 0;
					src.x = src.w * 3;
					break;
			}
			dst.y = height * dst.h;
			dst.x = width * dst.w;
			SDL_BlitSurface(zone->tileset, &src, screen, &dst);
		}
	}
	
	for (i = 0; i < 12; i++)
	{
		if (!coins[i]->get && coins[i]->zone == zone)
		{
			SDL_BlitSurface(images->misc, &coins[i]->animation, screen, &coins[i]->position);
		}
	}	

	for (i = 0; i < 7; i++)
	{
		if (monsters[i]->life && monsters[i]->zone == zone)
		{
			SDL_BlitSurface(player->sprite, &monsters[i]->animation, screen, &monsters[i]->position);
		}
	}

	SDL_BlitSurface(player->sprite, &player->animation, screen, &player->position);
}

