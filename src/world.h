#ifndef HAVE_WORLD_H
#define HAVE_WORLD_H

#include <stdlib.h>

typedef struct world_s world_t;
typedef struct coin_s coin_t;

#include "zone.h"

struct world_s
{
	zone_t *start_zone;
	zone_t *map[7][7];
	int	ways;
};

struct coin_s
{
	SDL_Surface *sprite;
	zone_t *zone;
	SDL_Rect position;
	SDL_Rect animation;
	int get;
};

world_t *new_world(void);
world_t *world_create(world_t *world);
void world_expend(world_t *world, int height, int width, int level);
void world_open_way(world_t *world);
void world_place_coins(world_t *world);
void world_place_monsters(world_t *world);

world_t *world;

#endif

