#ifndef HAVE_DISPLAY_H
#define HAVE_DISPLAY_H

#include <SDL/SDL.h>

typedef struct images_s images_t;

#include "dunscape.h"
#include "zone.h"
#include "command.h"

void new_display(void);
void display_refresh(void);
void display_load_images(void);
void display_print_zone(zone_t *zone);
void display_interface(void);
void display_life(void);
void display_coins(void);

struct images_s
{
	SDL_Surface *player;
	SDL_Surface *interface;
	SDL_Surface *misc;
	SDL_Surface *desert;
	SDL_Surface *plain;
	SDL_Surface *iceland;
	SDL_Surface *village;
	SDL_Surface *victory;
	SDL_Surface *fail;
};

SDL_Surface *screen;
images_t *images;

#endif

