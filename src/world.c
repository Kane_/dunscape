#include "world.h"

world_t *new_world(void)
{
	world_t *world = NULL;
	
	world = calloc(1, sizeof(world_t));
	world = world_create(world);
	
	return world;
}

world_t *world_create(world_t *world)
{
	world->map[3][3] = new_zone();
	world->ways = 5;
	
	world_expend(world, 3, 3, 3);
	world_open_way(world);
	world_place_coins(world);
	world_place_monsters(world);
	
	return world;
}

void world_expend(world_t *world, int height, int width, int level)
{
	if (level)
	{
		if (!world->map[height - 1][width] && rand() % 100 < 75)
		{
			world->map[height - 1][width] = new_zone();
			world->map[height][width]->top = world->map[height - 1][width];
			world->map[height - 1][width]->bottom = world->map[height][width];
			world_expend(world, height - 1, width, level - 1);
		}
		
		if (!world->map[height + 1][width] && rand() % 100 < 75)
		{
			world->map[height + 1][width] = new_zone();
			world->map[height][width]->bottom = world->map[height + 1][width];
			world->map[height + 1][width]->top = world->map[height][width];
			world_expend(world, height + 1, width, level - 1);
		}

		if (!world->map[height][width - 1] && rand() % 100 < 75)
		{
			world->map[height][width - 1] = new_zone();
			world->map[height][width]->left = world->map[height][width - 1];
			world->map[height][width - 1]->right = world->map[height][width];
			world_expend(world, height, width - 1, level - 1);
		}

		if (!world->map[height][width + 1] && rand() % 100 < 75)
		{
			world->map[height][width + 1] = new_zone();
			world->map[height][width]->right = world->map[height][width + 1];
			world->map[height][width + 1]->left = world->map[height][width];
			world_expend(world, height, width + 1, level - 1);
		}
	}
}

void world_open_way(world_t *world)
{
	register int height, width;

	for (height = 0; height < 7; height++)
	{
		for (width = 0; width < 7; width++)
		{
			if (world->map[height][width])
			{
				zone_open_way(world, world->map[height][width]);
			}
		}
	}
}

void world_place_coins(world_t *world)
{
	register int height, width;
	int coins, done;
	
	coins = 0;
	while (coins < 12)
	{
		for (height = 0; height < 7; height++)
		{
			for (width = 0; width < 7; width++)
			{
				if (world->map[height][width] && rand() % 50 < 2 && !done)
				{
					if (zone_place_coin(world->map[height][width], coins))
					{
						coins++;
						done = 1;
					}
				}
			}
		}
		done = 0;
	}
}

void world_place_monsters(world_t *world)
{
	register int height, width;
	int mon, done;
	
	mon = 0;
	while (mon < 7)
	{
		for (height = 0; height < 7; height++)
		{
			for (width = 0; width < 7; width++)
			{
				if (world->map[height][width] && rand() % 50 < 2 && !done)
				{
					if (zone_place_monsters(world->map[height][width], mon))
					{
						mon++;
						done = 1;
					}
				}
			}
		}
		done = 0;
	}
}

