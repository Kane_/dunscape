EXEC= dunscape
CC= gcc
CFLAGS= -ansi -pedantic -pedantic-errors
LFLAGS= -lSDL
WARN= -Wall -Wextra -Werror
DEBUG= -g

$(EXEC):		main.o display.o command.o zone.o world.o
						@echo link: $@
						@$(CC) -o $@ $^ $(LFLAGS)

main.o:			src/main.c src/dunscape.h
						@echo making: $@
						@$(CC) -c -o $@ $< $(CFLAGS) $(WARN) $(DEBUG)

display.o:	src/display.c src/display.h
						@echo making: $@
						@$(CC) -c -o $@ $< $(CFLAGS) $(WARN) $(DEBUG)

command.o:	src/command.c src/command.h src/display.h
						@echo making: $@
						@$(CC) -c -o $@ $< $(CFLAGS) $(WARN) $(DEBUG)

zone.o:			src/zone.c src/zone.h src/display.h
						@echo making: $@
						@$(CC) -c -o $@ $< $(CFLAGS) $(WARN) $(DEBUG)

world.o:		src/world.c src/world.h src/zone.h
						@echo making: $@
						@$(CC) -c -o $@ $< $(CFLAGS) $(WARN) $(DEBUG)

clean:
						@rm -v *.o $(EXEC)

